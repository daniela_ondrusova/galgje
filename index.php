<?php
setcookie("word", "", time() - 3600);
setcookie("characters", "", time() - 3600);
setcookie("mistakes", "", time() - 3600);
?>
<!DOCTYPE html>
<html lang="nl">

<head>
    <meta charset="utf-8">
    <title>Galgje</title>
</head>

<body>
    <h1>Galgje</h1>
    <form action="random.php" method="post">
        <label for='woord'>Woord (optioneel)</label><br />
        <input id='woord' type='text' name='woord' /><br />
        <br />
        <button type="submit" name="random">Start</button>
    </form>
</body>

</html>