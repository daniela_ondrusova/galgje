# Galgje Spel - PHP Together

Voor de module 'PHP Together' hebben wij het spel Galgje gebouwd. Je kunt zelf een woord toevoegen of een woord genereren door het programma.

## Hoe werkt het?

1. **Download de applicatie**
2. **Stel je MAMP/XAMPP/WAMP in op de gedownloade folder**
3. **Start de Apache-server**
4. **Ga naar de volgende URL: `localhost:PORT/index.php`**
5. **Type een woord of klik op start om direct te beginnen**
6. **Je hebt 8 kansen. Als je weer terug wilt naar `index.php`, klik dan op `reset`**

Veel speelplezier!