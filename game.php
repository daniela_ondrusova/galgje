<?php
if (isset($_POST['button'])) {
    if ($_POST['button'] != 'reset') {
        $lastCharacter = $_POST['button'];
        if (isset($_COOKIE['characters'])) {
            $characters = $_COOKIE['characters'] . ',' . $_POST['button'];
        } else {
            $characters = $_POST['button'];
        }
        setcookie('characters', $characters, time() + (86400 * 10));
        header("Location: game.php");
    } else {
        setcookie("word", "", time() - 3600);
        setcookie("characters", "", time() - 3600);
        setcookie("mistakes", "", time() - 3600);
        header("Location: index.php");
    }
}
?>
<!DOCTYPE html>
<html lang="nl">
<head>
    <meta charset="utf-8">
    <title>Galgje</title>
    <style>
        .mid {
            display: inline-block;
            width: 55%;
        }

        .right {
            display: inline-block;
            width: 44%;
        }

        .right img {
            width: 75%;
        }

        button {
            width: 50px;
            height: 50px;
        }

        form {
            width: 475px;
        }

        button:disabled {
            background-color: red;
            color: white;
        }
    </style>
</head>
<body>
<div class="mid">
    <h1>Galgje</h1>
    <?php
    $mistakesCount = 0;

    $wordCharacters = str_split(strtolower($_COOKIE['word']));

    if (isset($_COOKIE['characters'])) {
        $choiceCharacters = explode(',', $_COOKIE['characters']);
    } else {
        $choiceCharacters = array();
    }

    $won = true;

    foreach ($wordCharacters as $wordCharacter) {
        $choiceCorrect = false;
        foreach ($choiceCharacters as $choiceCharacter) {
            if ($wordCharacter === $choiceCharacter) {
                $choiceCorrect = true;
            }
        }
        if ($choiceCorrect) {
            echo($wordCharacter);
        } else {
            echo('_');
            $won = false;
        }
    }

    foreach ($choiceCharacters as $choiceCharacter) {
        $choiceCorrect = false;
        foreach ($wordCharacters as $wordCharacter) {
            if ($wordCharacter === $choiceCharacter) {
                $choiceCorrect = true;
            }
        }

        if (!$choiceCorrect) {
            $mistakesCount++;
        }
    }

    $lose = false;

    if ($mistakesCount === 8) {
        $lose = true;
    }

    if ($won) {
        echo '<br>' . '<h2>You Won</h2>';
    }

    if ($lose) {
        echo '<br>' . '<h2>You Lose</h2>' . '<br>' . '<p>The word was: </p>' . $_COOKIE['word'];
    }
    ?>
    <br>
    <br>
    <br>
    <form action="game.php" method="post">
        <button type="submit" name="button" value="reset">reset</button>
        <?php

        $alphabet = array('a', 'b', 'c', 'd', 'e', 'f', 'g', 'h', 'i', 'j', 'k', 'l', 'm', 'n', 'o', 'p', 'q', 'r', 's', 't', 'u', 'v', 'w', 'x', 'y', 'z', ' ');

        foreach ($alphabet as $value) {
            $display = true;
            foreach ($choiceCharacters as $choiceCharacter) {
                if ($value === $choiceCharacter) {
                    $display = false;
                }
            }
            if ($won) {
                $display = false;
            }
            if ($lose) {
                $display = false;
            }
            if ($display) {
                echo('<button type="submit" name="button" value="' . $value . '">' . $value . '</button>');
            } else {
                echo('<button type="submit" name="button" value="' . $value . '" disabled>' . $value . '</button>');
            }

        }

        ?>

    </form>

    <h1>Gebruikte karakters:</h1>
    <p>
        <?php
        foreach ($choiceCharacters as $choiceCharacter) {
            echo($choiceCharacter . ' , ');
        }
        ?>
    </p>
</div>
<div class="right">
    <?php
    if ($mistakesCount === 0) {
        echo('<img src="img/foto0.png">');
    } elseif ($mistakesCount === 1) {
        echo('<img src="img/foto1.png">');
    } else {
        echo('<img src="img/foto' . $mistakesCount . '.png">');
    }
    ?>
</div>
</body>
</html>